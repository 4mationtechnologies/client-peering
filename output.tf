#--------------------------------------------------------------
# Output
#--------------------------------------------------------------
output "peer_connection_id" {
  value = aws_vpc_peering_connection.default.id
}

output "peer_connection_accept_status" {
  value = aws_vpc_peering_connection.default.accept_status
}

