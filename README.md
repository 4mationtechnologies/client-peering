Terraform module to provision a AWS Peering connection between a client VPC and a 4mation VPC to allow easier management of client AWS resources.

## Module Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|----------|
|client_code|Unique Customer code, usually jira key|string|-|yes|
|environment|VPC environment|string|-|yes|
|peer_vpc_id|Destination VPC ID|string|-|yes|
|vpc_id|Requester VPC ID|string|-|yes|
|peering_vpc_route_table_ids|List of routing table IDs to add routes to|list|-|yes|
|destination_cidr|Destination CIDR for routing|string|172.31.0.0/20|no|
|peer_owner_id|The AWS account ID of the owner of the peer VPC|string||no|
|peer_region_id|The region of the peer owner|string|-|no|

## Module Outputs

| Name | Description |
|------|-------------|
|peer_connection_id|The ID of the VPC Peering Connection.|
|peer_connection_accept_status|The status of the VPC Peering Connection request.|