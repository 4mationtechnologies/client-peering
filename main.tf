#--------------------------------------------------------------
#
# 4mation Client Peering Module
#
#--------------------------------------------------------------

#--------------------------------------------------------------
# Create a peering connection to 4mation VPC
#--------------------------------------------------------------
resource "aws_vpc_peering_connection" "default" {
  peer_owner_id = var.peer_owner_id
  peer_vpc_id   = var.peer_vpc_id
  vpc_id        = var.vpc_id
  peer_region   = var.peer_region_id

  tags = {
    terraform   = "true"
    client      = var.client_code
    environment = var.environment
    Name        = "Peering between 4mation and ${var.client_code} ${var.environment}"
  }
}

#--------------------------------------------------------------
# Add route for 4mation CIDR to go to VPC Peering Inferface
#--------------------------------------------------------------
resource "aws_route" "default" {
  # One route per route table
  count = length(var.peering_vpc_route_table_ids)
  route_table_id            = element(var.peering_vpc_route_table_ids, count.index)
  destination_cidr_block    = var.destination_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.default.id
  depends_on                = [aws_vpc_peering_connection.default, var.peering_vpc_route_table_ids]
}