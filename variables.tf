#--------------------------------------------------------------
# Variables
#--------------------------------------------------------------

variable "peer_owner_id" {
  description = "The AWS account ID of the owner of the peer VPC."
}

variable "peer_vpc_id" {
  description = "Destination VPC ID."
}

variable "peer_region_id" {
  description = "Destination VPC Region"
  type = string
  default = null
}

variable "vpc_id" {
  description = "Requester VPC ID."
}

variable "destination_cidr" {
  default     = "172.31.0.0/20"
  description = "Destination CIDR for routing."
}

variable "peering_vpc_route_table_ids" {
  type        = list(string)
  description = "List of routing table IDs to add routes to."
}

variable "client_code" {
  description = "Unique Customer code, usually jira key"
}

variable "environment" {
  description = "VPC environment"
}

